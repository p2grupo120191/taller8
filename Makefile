all: bin/estatico bin/dinamico

bin/estatico: obj/prueba.o obj/Ejemplo.o obj/automovil.o obj/estudiante.o obj/libro.o lib/libslaballoc.a 
	gcc -static -Wall -g obj/Ejemplo.o obj/automovil.o obj/estudiante.o obj/libro.o obj/prueba.o lib/libslaballoc.a -o bin/estatico -Llib -lslaballoc

bin/dinamico: obj/prueba.o obj/Ejemplo.o obj/automovil.o obj/estudiante.o obj/libro.o lib/libslaballoc.so
	gcc -Wall -g obj/Ejemplo.o obj/automovil.o obj/estudiante.o obj/libro.o obj/prueba.o lib/libslaballoc.so -o bin/dinamico -Llib -lslaballoc	
	
lib/libslaballoc.a: obj/slabAllocator.o
	ar rcs lib/libslaballoc.a obj/slabAllocator.o
 
lib/libslaballoc.so: obj/slabAllocator.o
	gcc -shared -fPIC -o lib/libslaballoc.so obj/slabAllocator.o

obj/prueba.o: src/prueba.c
	gcc -Wall -g -c -I include/ src/prueba.c -o obj/prueba.o

obj/Ejemplo.o: src/Ejemplo.c
	gcc -Wall -g -c -I include/ src/Ejemplo.c -o obj/Ejemplo.o

obj/automovil.o: src/automovil.c
	gcc -Wall -g -c -I include/ src/automovil.c -o obj/automovil.o

obj/estudiante.o: src/estudiante.c
	gcc -Wall -g -c -I include/ src/estudiante.c -o obj/estudiante.o

obj/libro.o: src/libro.c
	gcc -Wall -g -c -I include/ src/libro.c -o obj/libro.o

obj/slabAllocator.o: src/slabAllocator.c
	gcc -Wall -g -c -I include/ src/slabAllocator.c -o obj/slabAllocator.o

.PHONY: clean
.PHONY: run_estatico
.PHONY: run_dinamico 
clean:
	rm bin/estatico bin/dinamico obj/*.o lib/*.a lib/*.so

run_estatico:
	./bin/estatico
run_dinamico:
	./bin/dinamico

	
