#include <unistd.h>
#include <stdlib.h>
#include "objetos.h"

void crear_estudiante(void *ref, size_t tamano){
	Estudiante *buf=(Estudiante*)ref;
	buf->matricula=(char*)calloc(9,sizeof(char));
	buf->nombres_com=(char*)calloc(50,sizeof(char));
	buf->promedio=0.0f;
	buf->estado=0;
}


void eliminar_estudiante(void *ref, size_t tamano){
	Estudiante *buf = (Estudiante *)ref;
	free(buf->matricula);
	free(buf->nombres_com);
	
	
}
